/// <reference path="./../typings/node-telegram-bot-api.d.ts" />

process.env.NODE_ENV = 'development';

import RedisApi = require('./libs/redisApi');
import TelegramBot = require('node-telegram-bot-api');
import Bluebird = require('bluebird');
import Redis = require('redis');
import MySQL = require('mysql');
import Observer = require('./libs/observer');
import GoogleApi = require('./libs/googleMapsApi');
import Cluster = require('cluster');
import OS = require('os');
import messages = require('./messages');
import dataBase = require('./libs/mysqlApi');

const config = require('./../config.json'),
      redisClient: Redis.RedisClient = Redis.createClient(),
      bot: TelegramBot = new TelegramBot(config.token, {polling: true}),
      redisApi: RedisApi = new RedisApi(redisClient),
      geoTime: GoogleApi = new GoogleApi(config.mapsApi, config.timeZoneApi, 'ru'),
      numCPUs = OS.cpus().length,
      pool = MySQL.createPool({
          connectionLimit : 15,
          host: config.host,
          user: config.user,
          password: config.password,
          database: config.database
      }),
      db: dataBase = new dataBase(pool),
      observer: Observer = new Observer();

interface User {
    id: number, //telegramID
    time: string, //время уведомления
    timeZone: number, //часовой пояс
    plants: Plant[] //массив растений
};

interface Plant {
    name: string, //имя растения
    period: number, //сколько раз в месяц его надо поливать
    last: string //когда в последний раз поливали
};

//обычное сообщение, требующее от пользователя нужного формата входных данных
observer.on('classic', (msg: Message, level: string) => observer.emit('message', msg, level));
observer.on('classic', (msg: Message, level: string) => redisApi.setSession(msg.from.id, messages[level].session));

//просто отправка сообщения
observer.on('message', (msg: Message, level: string) => bot.sendMessage(msg.from.id, messages[level].text, messages[level].keyboard));

//пользователь ввёл растения
observer.on('plants', (msg: Message) => {
    redisApi.savePlants(msg.text, msg.from.id);
    //const needToSave: boolean = await redisApi.checkUserInDB(msg.from.id); //проверяем, сохраняли ли мы пользователя в БД
    //!needToSave ? observer.emit('saveToRedis', msg) : observer.emit('saveToDB', msg);
});

//сохраняем растения в Redis
observer.on('saveToRedis', (msg: Message) => {
    redisApi.savePlants(msg.text, msg.from.id);
});

//сохранение времени уведомлений в Redis
observer.on('time', (msg: Message) => redisApi.saveTime(msg.from.id, msg.text));

//сохранение часового пояса пользователя
observer.on('zone', async (msg: Message) => {
    geoTime.getTimeZone(msg.text).then((data: string) => {
        redisApi.saveTimeZone(msg.from.id, data);
        observer.emitArr(['classic', 'end'], msg, 'end');
    }).catch((err: Error) => {
        console.log('Залупа');
    });
});

//пользователь ввёл все данные
observer.on('end', (msg: Message) => {
    redisApi.getUserForSave(msg.from.id).then((user: User) => {
        db.saveAllData(user);
    }).catch((err: Error) => {
        throw new Error('Ошибка, блеать');
    });
});

//удаление всех данных из БД
observer.on('delete', (msg: Message) => db.deleteAllInform(msg.from.id));

bot.on('message', async (msg: Message) => {
     const id: number = msg.from.id, text: string = msg.text;
     if (text === '/start') {
         redisClient.del(`id${msg.from.id}`);
         db.deleteAllInform(msg.from.id);
         observer.emit('classic', msg, 'main');
     } else {
       const session: string = await redisApi.getSession(id);
       switch (session) {
           case 'main': //пользователь в главном меню
               switch (text) {
                   case 'Добавить растения':
                       observer.emit('classic', msg,  'add');
                   break;

                   case 'Время уведомления':
                       observer.emit('classic', msg, 'time'); 
                   break;

                   case 'Отписаться':
                       observer.emitArr(['classic', 'delete'], msg, 'delete'); 
                   break;

                   default:
                       observer.emit('message', msg, 'errorMain'); 
                   break;
               }
           break;

           case 'add': //добавление растений
               /[ a-z|а-я]+(-| -)[  а-я]+ ([0-9]|)+[ а-я]+/ig.test(text) ? 
               observer.emitArr(['classic', 'plants'], msg, 'validPlants') : 
               observer.emit('message', msg, 'errorPlants');
           break;

           case 'time': //ввод времени
               /[\d]+:[\d]+/g.test(text) ?
               observer.emitArr(['classic', 'time'], msg, 'validTime') :
               observer.emit('message', msg, 'errorTime'); 
           break;

           case 'zone': //ввод города пользователя для запоминания часового пояса
               observer.emit('zone', msg);
           break;
       };
    };   
});