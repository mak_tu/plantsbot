import cluster = require('cluster');
import os = require('os');

const count: number = os.cpus().length;

cluster.setupMaster({exec: 'main.js'});

for (let i = 0; i < count; i++) {
    cluster.fork();
}

cluster.on('exit', (worker: cluster.Worker) => {
    console.log(`worker ${worker.process.pid} died`); 
});

console.log('Кластерзаци работает');
