import TelegramBot = require('node-telegram-bot-api');

interface handler {
    name: string, //имя события
    func: Array<(msg: Message, level: string) => any> //обработчики события
}

interface handlers {
    handlerFunctions: handler[],
    on(event: string, func: (msg: Message, level: string) => any): void, //подписка на событие
    emit(event: string, msg: Message, level: string): void //генерирование одного события
    emitArr(events: string[], msg: Message, level?: string): void //генерирование нескольких событий
}


class Observer implements handlers {
    handlerFunctions: handler[]
    
    constructor() {
        this.handlerFunctions = [];
    }

    private getNeedEvent(event: string): handler{
        return this.handlerFunctions.find((el: any) => {return el.name === event});
    }

    public on(event: string, func: (msg: Message, level: string) => any): void {
        const needEvent = this.getNeedEvent(event);
        if (needEvent != undefined) {
            needEvent.func.push(func);
        } else {
            const handler: handler = {name: event, func: []};
            handler.func.push(func);
            this.handlerFunctions.push(handler);
        }
    }

    public emit(event: string, msg: Message, level?: string): void {
        const needEvent = this.getNeedEvent(event);
        needEvent.func.forEach((el: any) => {el(msg, level)});
    }

    public emitArr(events: string[], msg: Message, level?: string): void {
        try {
            for (let event of events) this.emit(event, msg, level);
        } catch (err) {
            throw err;
        }
    } 
}

export = Observer