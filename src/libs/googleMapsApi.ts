interface optionsRequest { //опции для запросов к Google API
    hostname: string,
    port: number,
    method: string,
    path?: string
};

interface coordinates {
    lat: number,
    lng: number
}

const options: optionsRequest = {
    hostname: 'maps.googleapis.com',
    port: 443,
    method: 'POST'
}

interface GoogleApiStruct {
    keyGeo: string, //ключ Google Maps API
    keyTime: string, //ключ Google Time Zone API
    options: optionsRequest, 
    language: string, //язык ответов
    https: any, //модуль https для отправки запросов

    getGeoCode(query: string): Promise<coordinates>, //перевод текстового адреса в широту и долготу
    getTimeZoneByCoord(lat: number, lng: number): Promise<number>, //получение часового пояса места по его координатам
    getTimeZone(query: string): Promise<string> //получение часового пояса места по его названию
}

class GoogleApi implements GoogleApiStruct {
    keyGeo: string
    keyTime: string
    options: optionsRequest
    language: string
    https: any

    constructor(keyGeo: string, keyTime: string, language: string) {
        this.keyGeo = keyGeo;
        this.keyTime = keyTime;
        this.language = language;
        this.https = require('https');
        this.options = {
            hostname: 'maps.googleapis.com',
            port: 443,
            method: 'POST'
        }
    }

    private sendHTTPSReq(request: optionsRequest): Promise<Buffer> { //отправление запросов
        const that: this = this;
        return new Promise((resolve: (data: Buffer) => void, reject: (err: Error) => void) => {
            const req = that.https.request(request, (res: any) => {
                let data: any[] = [];
                    res.on('data', (d: any) => {
                    data.push(d);
                }).on('end', () => {
                    const trash: Buffer = Buffer.concat(data);
                    resolve(trash);
                });
            });

            req.on('error', (err: Error) => {
                reject(err);
            });

            req.end();
        });
    }

    private translit(word: string): string {
        let a = {"Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"'","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"'","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"'","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"'","б":"b","ю":"yu"};
        return word.split('').map((char: string) => {
            return (<any>a)[char] || char;
        }).join("");
    }

    public getGeoCode(query: string): Promise<coordinates> {
        let options: optionsRequest = this.options;
        let path: string = query.replace(/ /g, '+'), that: this = this;
        path = this.translit(path);
        options.path = `https://maps.googleapis.com/maps/api/geocode/json?address=${path}&key=${that.keyGeo}`;
        return new Promise((resolve: (data: coordinates) => void, reject: (err: Error) => void) => {
            that.sendHTTPSReq(options).then(data => {
                const result = JSON.parse(data.toString()).results[0];
                if (result != undefined) {
                    resolve(result.geometry.bounds.northeast);
                } else {
                    reject(new Error('Не удалось получить координаты места'));
                }
            });
        });
    }

    public getTimeZoneByCoord(lat: number, lng: number): Promise<number> {
        let options: optionsRequest = this.options, that: this = this, time: number = (new Date()).getTime()/1000;
        options.path = `https://maps.googleapis.com/maps/api/timezone/json?location=${lat},${lng}&timestamp=${time}&key=${this.keyTime}`;
        return new Promise((resolve: (data: number) => void, reject: (err: Error) => void) => {
            that.sendHTTPSReq(options).then(data => {
                if (data != undefined) {
                    const zone: number = JSON.parse(data.toString()).rawOffset / 3600;
                    resolve(zone);
                } else {
                    reject(new Error('Не удалось получить часовой пояс'));
                }
            });
        });
    }

    public getTimeZone(query: string): Promise<string> {
        const that = this;
        return new Promise(async (resolve: (data: string) => void, reject: (err: Error) => void) => {
            try {
                const coord: coordinates = await that.getGeoCode(query),
                      zone: number = await that.getTimeZoneByCoord(coord.lat, coord.lng);
                resolve(zone.toString());
            } catch(err) {
                reject(err);
            };
        });
    }
};

export = GoogleApi;