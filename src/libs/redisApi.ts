import Redis = require('redis');

interface User {
    id: number, //telegramID
    time: string, //время уведомления
    timeZone: number, //часовой пояс
    plants: Plant[] //массив растений
};

interface Plant {
    name: string, //имя растения
    period: number, //сколько раз в месяц его надо поливать
    last: string //когда в последний раз поливали
};

interface redisInterface {
    redis: Redis.RedisClient,

    getSession(id: number): Promise<string> //получение сессии
    setSession(id: number, session: string): void, //сохранение сессии
    saveTime(id: number, time: string): void, // сохранение времени уведомлений
    saveTimeZone(id: number, zone: string): void, //сохранение часового пояса
    getUserForSave(id: number): Promise<User>, //получение всей информации о пользователе для сохранения его в БД
    getPlantsForSave(id: number): Promise<Plant[]>, //получение растений пользователя для сохранения их в БД
    savePlants(message: string, id: number): void, //сохранение растений в Redis
    checkUserInDB(id: number): Promise<boolean> //проверяем, был ли сохранён этот пользователь в БД
};

class RedisApi implements redisInterface {
    redis: Redis.RedisClient

    constructor(redis: Redis.RedisClient) {
        this.redis = redis;
    }

    public setSession(id: number, session: string): void {
        this.redis.hset(`id${id}`, 'session', session);        
    }

    public async getSession(id: number): Promise<string> {
        return new Promise((res: (session: string) => void, rej: (err: Error) => void) => {
            this.redis.hget(`id${id}`, 'session', (err: Error, data: string) => {
                err != undefined ? rej(err) : res(data);
            });    
        });
    }

    public saveTime(id: number, time: string): void {
        this.redis.hset(`id${id}`, 'time', time);  
    }

    public saveTimeZone(id: number, zone: string): void {
        this.redis.hset(`id${id}`, 'timeZone', zone);
    }

    public checkUserInDB(id: number): Promise<boolean> {
        const that = this;  
        return new Promise((res: (session: boolean) => void, rej: (err: Error) => void) => {
            that.redis.hget(`id${id}`, 'db', (err: Error, data: string) => {
                if (err !== undefined) {
                    data !== undefined ? res(true) : res(false);
                }
            });    
        });
    }

    public getUserForSave(id: number): Promise<User> {
        const that = this;  
        return new Promise((res: (data: User) => void, rej: (err: Error) => void) => {
            that.redis.hgetall(`id${id}`, (error: Error, data: any) => {
                const hours: number = parseInt(data.time.substr(0, 2)),
                      minutes: string = data.time.substr(3, 6);
                const user: User = {
                    id: id,
                    time: `${hours - parseInt(data.timeZone)}:${minutes}`,
                    timeZone: data.timeZone,
                    plants: JSON.parse(data.plants)
                };
                
                error === undefined ? rej(error) : res(user);
            });
        });  
    }

    public getPlantsForSave(id: number): Promise<Plant[]> {
        const that = this;
        return new Promise((res: (data: Plant[]) => void, rej: (err: Error) => void) => {
            that.redis.hget(`id${id}`, 'plants', (err: Error, data: any) => {
                err === undefined ? res(JSON.parse(data)) : rej(err);
            });
        });           
    }

    public savePlants(message: string, id: number): void {
        const names: string[] = this.getPlants(message),
              period: number[] = this.getPeriodicity(message),
              dimension: number[] = this.getDimension(message),
              plants: Plant[] = [];    
        
        for (let i in names) {
            const now: Date = new Date();
            now.setDate(now.getDate() + period[i] * dimension[i]);
            const plant: Plant = {
                name: names[i],
                period: period[i] * dimension[i],
                last: `${now.getUTCFullYear()}-${now.getUTCMonth()}-${now.getUTCDate()}`
            }
            plants.push(plant);
        }

        this.redis.hset(`id${id}`, 'plants', JSON.stringify(plants));        
    }

    private getPlants(msg: string): string[] { //получение массива имён растений
        let plants: RegExpMatchArray = msg.match(/([a-z |а-я ])+(-| -|- )/ig);

        plants = plants.map(plant => {
            let i = plant.indexOf(' -');
            return (i==-1) ? plant.replace('-', '') : plant.replace(' -', '');
        });

        return plants;
    }

    private getPeriodicity(message: string): number[] { //получение массива значений периодичности полива (например, [2, 5, 6])
        let numbers: number[] = [], array: string[] = [];

        message.indexOf('\n') == -1 ? array[0] = message : array = message.split('\n');
        array.forEach((el: any) => {
            let trash: string[] = el.match(/[2-9]/);
            trash ? numbers.push(parseInt(trash[0])) : numbers.push(1);
        });

        return numbers;
    }

    private getDimension(message: string): number[] {
        let dim: number[] = [], array: string[] = [];

        message.indexOf('\n') == -1 ? array[0] = message : array = message.split('\n');
        array.forEach((el: any) => {
            let trash: string[] = el.match(/[a-z|а-я]+/ig)[el.match(/[a-z|а-я]+/ig).length-1];
            trash.indexOf('месяц') === 0 ? dim.push(30) :
            trash.indexOf('д') === 0 ? dim.push(1) : dim.push(7);
        });

        return dim;
    }
};

export = RedisApi;