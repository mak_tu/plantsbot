import MySQL = require('mysql');

interface User {
    id: number, //telegramID
    time: string, //время уведомления
    timeZone: number, //часовой пояс
    plants: Plant[] //массив растений
};

interface Plant {
    name: string, //имя растения
    period: number, //сколько раз в месяц его надо поливать
    last: string //когда в последний раз поливали
};

interface sql {
    pool: MySQL.IPool,

    savePlants(user: User): void,
    saveUser(user: User): Promise<number>,
    saveAllData(user: User): void, //сохранение во все три таблицы данныe пользователя
    deleteAllInform(id: number): void //удаление всей информации из БД
};

class sqlApi implements sql {
    pool: MySQL.IPool

    constructor(pool: MySQL.IPool) {
        this.pool = pool;
    }

    public async saveAllData(user: User): Promise<void> {
        try {
            await this.saveUser(user);
            this.savePlants(user);
        } catch(err) {
            throw err;
        }   
    }

    public savePlants(user: User): void {
        const query = 'INSERT INTO plants (??, ??, ??, ??) VALUES (?, ?, ?, ?)';
        for (let plant of user.plants) {
            this.pool.query(query, ['name', 'period', 'next', 'telegramID', plant.name, plant.period, plant.last, user.id], (err: MySQL.IError, rows: any) => {
                if (err) throw err;
            });
        };
    }

    public saveUser(user: User): Promise<number> {
        return new Promise((res: (data: number) => void, rej: (err: MySQL.IError) => void) => {
            const query = 'INSERT INTO users (??, ??) VALUES (?, ?)';
            this.pool.query(query, ['telegramID', 'time', user.id, user.time], (err: MySQL.IError, rows: any) => {
                if (err) { 
                    rej(err);
                } else {
                    res(rows.insertId);
                }
            });
        });
    }

    public deleteAllInform(id: number): void {
        const query = 'DELETE from users WHERE ?? = ?';
        this.pool.query(query, ['telegramID', id], (err: MySQL.IError, rows: any, fields: any) => {
            if (err) throw err;
        });
    }
};

export = sqlApi;